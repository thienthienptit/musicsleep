package com.musicsleep.screen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.musicsleep.R;
import com.musicsleep.adapter.GridSoundViewAdapter;
import com.musicsleep.adapter.SoundViewAdapter;
import com.musicsleep.base.BaseFragment;
import com.musicsleep.customview.SoundViewPager;
import com.musicsleep.model.SoundModel;
import com.musicsleep.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SoundGroupConfigFragment extends BaseFragment {

    @BindView(R.id.rcv_sound_play)
    RecyclerView rcvSoundPlay;
    @BindView(R.id.pager_sound)
    SoundViewPager viewPagerSound;

    private List<SoundModel> listSoundConfig = new ArrayList<>();
    private OnChangeSound onChangeSound;
    private SoundViewAdapter adapter;
    Unbinder unbinder;

    public static SoundGroupConfigFragment newInstance(List<SoundModel> listSound, OnChangeSound onChangeSound) {

        Bundle args = new Bundle();

        SoundGroupConfigFragment fragment = new SoundGroupConfigFragment();
        fragment.setArguments(args);
        fragment.listSoundConfig = listSound;
        fragment.onChangeSound = onChangeSound;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sound_group_config, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUI();
        initControl();
        return view;
    }

    private void initControl() {

    }

    private void initUI() {
        viewPagerSound.setBaseController(this);
        viewPagerSound.initData();
        if (listSoundConfig.size() > 0)
            viewPagerSound.setListSoundDataSave(listSoundConfig);
        adapter = new SoundViewAdapter(getContext(), listSoundConfig);
        adapter.setOnClickItem(new SoundViewAdapter.OnClickItem() {
            @Override
            public void onChangeSeekbar(SoundModel soundModel) {

            }

            @Override
            public void onSelectPlay(int soundId) {

            }

            @Override
            public void addSound(SoundModel soundModel) {

            }

            @Override
            public void removeSound(SoundModel soundModel) {
                SoundGroupConfigFragment.this.removeSound(soundModel);
                listSoundConfig.remove(soundModel);
                viewPagerSound.initData();
                adapter.notifyDataSetChanged();
            }
        });
        rcvSoundPlay.setAdapter(adapter);
    }

    @Override
    public void addSound(SoundModel soundModel) {
        super.addSound(soundModel);
        listSoundConfig.add(soundModel);
        adapter.notifyDataSetChanged();
        if (onChangeSound != null)
            onChangeSound.onChange();
    }

    @Override
    public void removeSound(SoundModel soundModel) {
        super.removeSound(soundModel);
        listSoundConfig.remove(soundModel);
        adapter.notifyDataSetChanged();
        if (onChangeSound != null)
            onChangeSound.onChange();
    }

    @Override
    public void createSound(List<SoundModel> soundModels) {
        super.createSound(soundModels);
        if (getSoundQuantity() > 0) {
            listSoundConfig.clear();
            listSoundConfig.addAll(getListSoundPlay());
            adapter.notifyDataSetChanged();
        }
        if (onChangeSound != null)
            onChangeSound.onChange();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface OnChangeSound {
        void onChange();
    }

    public void setOnChangeSound(OnChangeSound onChangeSound) {
        this.onChangeSound = onChangeSound;
    }
}
