package com.musicsleep.screen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.musicsleep.R;
import com.musicsleep.activity.FunctionActivity;
import com.musicsleep.base.BaseFragment;
import com.musicsleep.model.SoundModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PlayGroupSoundFragment extends BaseFragment {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.btn_play)
    ImageButton btnPlay;
    Unbinder unbinder;
    @BindView(R.id.iv_sound_one)
    ImageView ivSoundOne;
    @BindView(R.id.iv_sound_two)
    ImageView ivSoundTwo;
    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.tv_sound_number)
    TextView tvQuantitySound;
    private List<SoundModel> listSoundConfig = new ArrayList<>();

    public static PlayGroupSoundFragment newInstance(List<SoundModel> listSoundConfig) {

        Bundle args = new Bundle();

        PlayGroupSoundFragment fragment = new PlayGroupSoundFragment();
        fragment.setArguments(args);
        fragment.listSoundConfig = listSoundConfig;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_play_group_sound, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUI();
        initControl();
        return view;
    }

    private void initControl() {

    }

    private void initUI() {
        if (listSoundConfig.size() > 0) {
            createSound(listSoundConfig);
            playSound();
        }
        checkMediaPlay();
    }

    public void checkMediaPlay() {
        tvQuantitySound.setText(getSoundQuantity() + "");
        if (isPlaySound()) {
            btnPlay.setImageResource(android.R.drawable.ic_media_pause);
        } else {
            btnPlay.setImageResource(android.R.drawable.ic_media_play);
        }
    }

    @Override
    public void createSound(List<SoundModel> soundModels) {
        super.createSound(soundModels);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_sound_one, R.id.iv_sound_two, R.id.iv_edit, R.id.btn_play})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_sound_one:
                break;
            case R.id.iv_sound_two:
                break;
            case R.id.iv_edit:
                ((FunctionActivity) getActivity()).addFragment(
                        SoundGroupConfigFragment
                                .newInstance(listSoundConfig, () -> {
                                    checkMediaPlay();
                                })
                );
                break;
            case R.id.btn_play:
                if (isPlaySound()) {
                    pauseSound();
                } else {
                    playSound();
                }
                checkMediaPlay();
                break;
        }
    }
}
