package com.musicsleep.screen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.musicsleep.R;
import com.musicsleep.adapter.FragmentPagerAdapter;
import com.musicsleep.base.BaseFragment;
import com.musicsleep.data.DataResource;
import com.musicsleep.data.RealmController;
import com.musicsleep.model.SoundCategoryModel;
import com.musicsleep.model.SoundGroupModel;
import com.musicsleep.utils.Toolbox;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;

public class CategorySoundContainerFragment extends BaseFragment {
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    Unbinder unbinder;

    private FragmentPagerAdapter adapter;
    private boolean touchFromUser;

    private List<SoundCategoryModel> listCategory = new ArrayList<>();

    public static CategorySoundContainerFragment newInstance() {

        Bundle args = new Bundle();

        CategorySoundContainerFragment fragment = new CategorySoundContainerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_sound, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUI();
        initControl();
        return view;
    }

    private void initControl() {

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                touchFromUser = true;
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (!touchFromUser) {
                    tabs.setScrollPosition(position, positionOffset, true);
                }
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == 0)
                    touchFromUser = false;
            }
        });
    }

    private void initUI() {
        listCategory.clear();
        listCategory.addAll(getSoundCategoryDatas());
        setupTabs();
    }

    private void setupTabs() {
        adapter = new FragmentPagerAdapter(getChildFragmentManager());
        for (SoundCategoryModel category:
             listCategory) {
            List<SoundGroupModel> soundGroup = Toolbox.getSoundGroupFromCategoryId(
                    (ArrayList<SoundGroupModel>) getSoundGroupDatas(), category.getId());
            if (-1 == category.getId()){
                soundGroup = getSoundGroupDatas();
            }
            if (soundGroup.size() > 0) {
                adapter.addFragment(CategorySoundFragment.newInstance(soundGroup), category.getName());
            }
        }
        viewpager.setAdapter(adapter);
        viewpager.setOffscreenPageLimit(adapter.getCount());
        for (int i = 0; i < adapter.getCount(); i++) {
            tabs.addTab(tabs.newTab());
            tabs.getTabAt(i).setCustomView(adapter.getTabView(i, getContext()));
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
