package com.musicsleep.screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.musicsleep.activity.FunctionActivity;
import com.musicsleep.R;
import com.musicsleep.adapter.GroupSoundAdapter;
import com.musicsleep.base.BaseFragment;
import com.musicsleep.base.FragmentSession;
import com.musicsleep.model.SoundGroupModel;
import com.musicsleep.model.SoundModel;
import com.musicsleep.utils.RecyclerItemClickListener;
import com.musicsleep.utils.Toolbox;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CategorySoundFragment extends BaseFragment {
    @BindView(R.id.rcv_sound)
    RecyclerView rcvSound;
    Unbinder unbinder;

    private List<SoundGroupModel> soundGroupModels = new ArrayList<>();

    private GroupSoundAdapter adapter;

    public static CategorySoundFragment newInstance(List<SoundGroupModel> soundGroupModels) {

        Bundle args = new Bundle();

        CategorySoundFragment fragment = new CategorySoundFragment();
        fragment.setArguments(args);
        fragment.soundGroupModels = soundGroupModels;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.category_sound_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUI();
        initControl();
        return view;
    }

    private void initControl() {
        rcvSound.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
            List<SoundModel> listSound = new ArrayList<>();
            SoundGroupModel soundGroup = soundGroupModels.get(position);
            String[] soundId = soundGroup.getListSoundId().split(",");
            for (int i = 0; i < soundId.length; i++) {
                listSound.add(Toolbox.getSoundFromId(Integer.parseInt(soundId[i]), (ArrayList<SoundModel>) getSoundDatas()));
            }
            Intent intent = new Intent(getActivity(), FunctionActivity.class);
            FragmentSession.getInstance().setData(PlayGroupSoundFragment.newInstance(listSound));
            startActivity(intent);
        }));
    }

    private void initUI() {
        rcvSound.setLayoutManager(new GridLayoutManager(getContext(),2, LinearLayoutManager.VERTICAL,false));
        adapter = new GroupSoundAdapter(getContext(), soundGroupModels);
        rcvSound.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
