package com.musicsleep.screen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.musicsleep.R;
import com.musicsleep.base.BaseFragment;
import com.musicsleep.base.BaseActivity;
import com.musicsleep.customview.SoundViewPager;
import com.musicsleep.model.SoundModel;
import com.musicsleep.utils.PreferenceUtils;
import com.musicsleep.utils.SoundManager;
import com.musicsleep.utils.Toolbox;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SoundHomeFragment extends BaseFragment {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.view_pager)
    SoundViewPager viewPagerSound;
    Unbinder unbinder;
    @BindView(R.id.btn_play)
    ImageButton btnPlay;

    @BindView(R.id.tv_quantity_sound)
    TextView tvQuantitySound;

    public static SoundHomeFragment newInstance() {

        Bundle args = new Bundle();

        SoundHomeFragment fragment = new SoundHomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sound_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        initUI();
        initControl();
        return view;
    }

    private void initControl() {
        btnPlay.setOnClickListener(v -> {
            if (isPlaySound()) {
                pauseSound();
            } else {
                playSound();
            }
            checkMediaPlay();
        });
    }

    private void initUI() {
        viewPagerSound.setBaseController(this);
        viewPagerSound.initData();
        tvTitle.setText("Âm thanh ngủ");
        viewPagerSound.setListSoundDataSave(Toolbox.getSoundDataPlay());
        checkMediaPlay();
    }

    @Override
    public void createSound(List<SoundModel> soundModels) {
        super.createSound(soundModels);
        checkMediaPlay();
        Toolbox.compressedSoundData(getListSoundPlay());
    }

    @Override
    public void addSound(SoundModel soundModel) {
        super.addSound(soundModel);
        checkMediaPlay();
        Toolbox.compressedSoundData(getListSoundPlay());
    }

    @Override
    public void removeSound(SoundModel soundModel) {
        super.removeSound(soundModel);
        checkMediaPlay();
        Toolbox.compressedSoundData(getListSoundPlay());
    }

    @Override
    public void pauseSound() {
        super.pauseSound();
        checkMediaPlay();
    }

    public void checkMediaPlay() {
        tvQuantitySound.setText(getSoundQuantity() > 0
                ? getSoundQuantity() + "" : "");
        if (isPlaySound()) {
            btnPlay.setImageResource(android.R.drawable.ic_media_pause);
        } else {
            btnPlay.setImageResource(android.R.drawable.ic_media_play);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
