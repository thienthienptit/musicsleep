package com.musicsleep.customview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.musicsleep.R;
import com.musicsleep.adapter.GridSoundViewAdapter;
import com.musicsleep.base.BaseController;
import com.musicsleep.model.SoundModel;
import com.musicsleep.adapter.ViewPagerAdapter;
import com.musicsleep.utils.Toolbox;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SoundViewPager extends LinearLayout {
    @BindView(R.id.view_pager_sound)
    ViewPager viewPager;

    public static final int GRID_NUMBER_ITEM = 9;
    public static final int COLUMN_NUMBER = 3;

    private ViewPagerAdapter vpAdapter;

    private List<SoundModel> listSoundData = new ArrayList<>();
    private List<GridView> gridList = new ArrayList<>();

    private GridSoundViewAdapter adapter;

    private BaseController baseController;

    public SoundViewPager(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.sound_view_pager, this);
        ButterKnife.bind(this, view);
        initUI();
    }

    public void initData() {
        listSoundData.clear();
        listSoundData.addAll(baseController.getSoundDatas());

        if (baseController.getSoundQuantity() > 0 && baseController.isPlayNormal()){
            for (SoundModel soundPlay :
                    baseController.getListSoundPlay()) {
                for (int i = 0; i < listSoundData.size(); i++) {
                    if (soundPlay.getId() == listSoundData.get(i).getId()){
                        listSoundData.set(i, soundPlay);
                    }
                }
            }
        }

        setUpViewPager();
    }

    public void setUpViewPager(){
        int pageSize = listSoundData.size() % GRID_NUMBER_ITEM == 0
                ? listSoundData.size() / GRID_NUMBER_ITEM
                : (listSoundData.size() / GRID_NUMBER_ITEM + 1);
        for (int i = 0; i < pageSize; i++) {
            GridView gridView = new GridView(getContext());
            gridView.setGravity(Gravity.CENTER);
            adapter = new GridSoundViewAdapter(listSoundData, i);
            adapter.setOnClickItem(new GridSoundViewAdapter.OnClickItem() {
                @Override
                public void onChangeSeekbar(SoundModel soundModel) {

                }

                @Override
                public void onSelectPlay(int soundId) {

                }

                @Override
                public void addSound(SoundModel soundModel) {
                    if (baseController != null)
                        if (!baseController.isPlayNormal()) {
                            baseController.createSound(Toolbox.getListSoundSave(listSoundData, Toolbox.getSoundDataPlay()), true);
                        }
                        baseController.addSound(soundModel);
                    initData();
                }

                @Override
                public void removeSound(SoundModel soundModel) {
                    if (baseController != null)
                        if (!baseController.isPlayNormal()) {
                            baseController.createSound(Toolbox.getListSoundSave(listSoundData, Toolbox.getSoundDataPlay()), true);
                        }
                        baseController.removeSound(soundModel);
                    initData();
                }
            });
            gridView.setNumColumns(COLUMN_NUMBER);
            gridView.setAdapter(adapter);
            gridList.add(gridView);
        }

        vpAdapter.add(gridList);
    }

    private void initUI() {
        vpAdapter = new ViewPagerAdapter();
        viewPager.setAdapter(vpAdapter);
    }

    public void setListSoundDataSave(List<SoundModel> listSoundDataSave){
        if (Toolbox.getSoundDataPlay() != null && baseController.getSoundQuantity() == 0
                && !baseController.isPlaySound()) {
            baseController.createSound(Toolbox.getListSoundSave(listSoundData, listSoundDataSave));
            adapter.notifyDataSetChanged();
        }
    }

    public void setBaseController(BaseController baseController) {
        this.baseController = baseController;
    }
}
