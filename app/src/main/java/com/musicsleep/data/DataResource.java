package com.musicsleep.data;

import android.content.Context;

import com.musicsleep.data.enum_sound.SoundCategoryEnum;
import com.musicsleep.data.enum_sound.SoundEnum;
import com.musicsleep.data.enum_sound.SoundGroupEnum;
import com.musicsleep.data.realm_object.SoundGroupObject;
import com.musicsleep.model.SoundCategoryModel;
import com.musicsleep.model.SoundGroupModel;
import com.musicsleep.model.SoundModel;
import com.musicsleep.utils.Toolbox;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class DataResource {
    private Context context;
    private Realm realm;
    private List<SoundGroupModel> soundGroupDatas = new ArrayList<>();
    private List<SoundCategoryModel> soundCategoryDatas = new ArrayList<>();

    public DataResource(Context context) {
        this.context = context;
        realm = RealmController.getSoundsRealm();
        setSoundGroupDatas(getSoundGroupConfig());
        setSoundCategoryDatas(getSoundCategory());
    }

    public List<SoundModel> getDataSound(){
        List<SoundModel> listSound = new ArrayList<>();
        for (int i = 0; i < SoundEnum.values().length; i++) {
            SoundEnum soundEnum = SoundEnum.values()[i];
            listSound.add(new SoundModel(i, null, soundEnum.getSoundName(), soundEnum.getSoundIconResId(),
                    "1", soundEnum.getSoundResId(), "", "", 0.5F, false));
        }
        return listSound;
    }

    public List<SoundCategoryModel> getSoundCategory(){
        List<SoundCategoryModel> listSoundCategory = new ArrayList<>();
        for (SoundCategoryEnum categoryEnum :
                SoundCategoryEnum.values()) {
            SoundCategoryModel categoryModel = new SoundCategoryModel(categoryEnum);
            listSoundCategory.add(categoryModel);
        }

        return listSoundCategory;
    }
    public void initData(){
        List<SoundGroupModel> soundDatas = new ArrayList<>();
        for (int i = 0; i < SoundGroupEnum.values().length; i++) {
            soundDatas.add(new SoundGroupModel(SoundGroupEnum.values()[i]));
        }

        if (getSoundGroupConfig().size() < soundDatas.size()){
            for (SoundGroupModel sound:
                 soundDatas) {
                RealmController.insertSoundGroup(realm, sound);
            }
        }
    }

    public List<SoundGroupModel> getSoundGroupConfig(){
        List<SoundGroupModel> soundDatas = new ArrayList<>();
        RealmResults<SoundGroupObject> realmObject = realm.where(SoundGroupObject.class).findAll();
        if (realmObject != null && realmObject.size() > 0){
            for (SoundGroupObject sound :
                    realmObject) {
                SoundGroupModel soundGroupModel = new SoundGroupModel(sound);
                soundDatas.add(soundGroupModel);
            }
        }
        return soundDatas;
    }

    public List<SoundGroupModel> getSoundGroupDatas() {
        return soundGroupDatas;
    }

    public void setSoundGroupDatas(List<SoundGroupModel> soundGroupDatas) {
        this.soundGroupDatas = soundGroupDatas;
    }

    public List<SoundCategoryModel> getSoundCategoryDatas() {
        return soundCategoryDatas;
    }

    public void setSoundCategoryDatas(List<SoundCategoryModel> soundCategoryDatas) {
        this.soundCategoryDatas = soundCategoryDatas;
    }
}
