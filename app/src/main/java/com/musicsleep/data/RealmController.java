package com.musicsleep.data;

import com.musicsleep.data.realm_module.SoundGroupModule;
import com.musicsleep.data.realm_object.SoundGroupObject;
import com.musicsleep.model.SoundGroupModel;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmController {
    public static Realm getSoundsRealm(){
        RealmConfiguration myConfig = new RealmConfiguration.Builder()
                .modules(new SoundGroupModule())
                .name("movie.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        return Realm.getInstance(myConfig);
    }

    public static void insertSoundGroup(final Realm realmSoundGroup, final SoundGroupModel sound){
        final SoundGroupObject soundGroupObject = new SoundGroupObject(sound);
        realmSoundGroup.beginTransaction();
        realmSoundGroup.insertOrUpdate(soundGroupObject);
        realmSoundGroup.commitTransaction();
    }
}
