package com.musicsleep.data.enum_sound;

import com.musicsleep.R;

public enum  SoundGroupEnum {
    SOUND_GROUP1(1, "Trong nhà", R.mipmap.ic_launcher, 1, "1,2"),
    SOUND_GROUP2(2, "Ngoài trời", R.mipmap.ic_launcher, 2, "1,2"),
    SOUND_GROUP3(3, "Trong bếp", R.mipmap.ic_launcher, 3, "1,2"),
    SOUND_GROUP4(4, "Trong bếp", R.mipmap.ic_launcher, 3, "1,2"),
    ;
    private int id;
    private String name;
    private int avatar;
    private int categoryId;
    private String listSoundId;

    SoundGroupEnum(int id, String name, int avatar, int categoryId, String listSoundId) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.categoryId = categoryId;
        this.listSoundId = listSoundId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getListSoundId() {
        return listSoundId;
    }

    public void setListSoundId(String listSoundId) {
        this.listSoundId = listSoundId;
    }
}
