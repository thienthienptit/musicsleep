package com.musicsleep.data.enum_sound;

import com.musicsleep.R;

public enum SoundEnum {
    SOUND_ENUM1(1, "Tiếng chim", 0, "" , R.raw.bird),
    SOUND_ENUM2(2, "Hang động", 0, "" , R.raw.cave),
    SOUND_ENUM3(3, "Tiếng lửa", 0, "" , R.raw.fire),
    SOUND_ENUM4(4, "Xe lửa", 0, "" , R.raw.train),
    SOUND_ENUM5(5, "Chuyến bay", 0, "" , R.raw.flight),
    SOUND_ENUM6(6, "Gió", 0, "" , R.raw.wind),
    SOUND_ENUM7(7, "Tiếng chim", 0, "" , R.raw.bird),
    SOUND_ENUM8(8, "Hang động", 0, "" , R.raw.cave),
    SOUND_ENUM9(9, "Tiếng lửa", 0, "" , R.raw.fire),
    SOUND_ENUM10(10, "Xe lửa", 0, "" , R.raw.train),
    SOUND_ENUM11(11, "Chuyến bay", 0, "" , R.raw.flight),
    SOUND_ENUM12(12, "Gió", 0, "" , R.raw.wind),
    SOUND_ENUM13(13, "Tiếng chim", 0, "" , R.raw.bird),
    SOUND_ENUM14(14, "Hang động", 0, "" , R.raw.cave),
    SOUND_ENUM15(15, "Tiếng lửa", 0, "" , R.raw.fire),
    SOUND_ENUM16(16, "Xe lửa", 0, "" , R.raw.train),
    SOUND_ENUM17(17, "Chuyến bay", 0, "" , R.raw.flight),
    SOUND_ENUM18(18, "Gió", 0, "" , R.raw.wind)
    ;
    private int id;
    private String soundName;
    private int soundIconResId;
    private String soundIconUrl;
    private int soundResId;

    SoundEnum(int id, String soundName, int soundIconResId, String soundIconUrl, int soundResId) {
        this.id = id;
        this.soundName = soundName;
        this.soundIconResId = soundIconResId;
        this.soundIconUrl = soundIconUrl;
        this.soundResId = soundResId;
    }

    SoundEnum(int id) {
        this.id = id;
    }

    public String getSoundName() {
        return soundName;
    }

    public void setSoundName(String soundName) {
        this.soundName = soundName;
    }

    public int getSoundIconResId() {
        return soundIconResId;
    }

    public void setSoundIconResId(int soundIconResId) {
        this.soundIconResId = soundIconResId;
    }

    public String getSoundIconUrl() {
        return soundIconUrl;
    }

    public void setSoundIconUrl(String soundIconUrl) {
        this.soundIconUrl = soundIconUrl;
    }

    public int getSoundResId() {
        return soundResId;
    }

    public void setSoundResId(int soundResId) {
        this.soundResId = soundResId;
    }
}
