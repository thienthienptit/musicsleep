package com.musicsleep.data.enum_sound;

import com.musicsleep.MyApplication;
import com.musicsleep.R;
import com.musicsleep.utils.Toolbox;

public enum SoundCategoryEnum {
    SOUND_CATEGORY_ENUM1(-1, "Tất cả", R.mipmap.ic_launcher),
    SOUND_CATEGORY_ENUM2(0, "Tùy chỉnh", R.mipmap.ic_launcher),
    SOUND_CATEGORY_ENUM3(1, "Ngủ", R.mipmap.ic_launcher),
    SOUND_CATEGORY_ENUM4(2, "Mưa rơi", R.mipmap.ic_launcher),
    SOUND_CATEGORY_ENUM5(3, "Thư giãn", R.mipmap.ic_launcher),
    SOUND_CATEGORY_ENUM6(4, "Thiền", R.mipmap.ic_launcher),
    SOUND_CATEGORY_ENUM7(5, "Làm việc", R.mipmap.ic_launcher),
    ;
    private int id;
    private String name;
    private int iconResId;

    SoundCategoryEnum(int id, String name, int iconResId) {
        this.id = id;
        this.name = name;
        this.iconResId = iconResId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIconResId() {
        return iconResId;
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
    }
}
