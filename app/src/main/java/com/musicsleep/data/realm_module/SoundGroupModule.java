package com.musicsleep.data.realm_module;

import com.musicsleep.data.realm_object.SoundGroupObject;

import io.realm.annotations.RealmModule;

@RealmModule(classes = SoundGroupObject.class)
public class SoundGroupModule {
}
