package com.musicsleep.utils;

import android.content.SharedPreferences;

import com.musicsleep.MyApplication;
import com.musicsleep.model.SoundModel;

import java.util.List;

public class PreferenceUtils {
    public static final String SHARED_DEFAULT = "Shared_pre";
    public static final String SOUND_MEDIA_PLAY = "sound_media";

    public static SharedPreferences getPreference(){
        return MyApplication.getInstance().getSharedPreferences(SHARED_DEFAULT, 0);
    }

    public static void setSoundPlayData(String soundPlayData){
        SharedPreferences.Editor editor = getPreference().edit();
        editor.putString(SOUND_MEDIA_PLAY, soundPlayData);
        editor.commit();
    }

    public static String getSoundPlayData(){
        return getPreference().getString(SOUND_MEDIA_PLAY, "");
    }
}
