package com.musicsleep.utils;

import android.content.SharedPreferences;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.musicsleep.MyApplication;
import com.musicsleep.data.DataResource;
import com.musicsleep.data.enum_sound.SoundEnum;
import com.musicsleep.data.realm_object.SoundGroupObject;
import com.musicsleep.model.SoundGroupModel;
import com.musicsleep.model.SoundModel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class Toolbox {
    private static Gson gson;

    static {
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
    }

    public static Gson getGson(){
        return gson;
    }

    public static void compressedSoundData(List<SoundModel> soundData) {
        String sounds = getGson().toJson(soundData);
        PreferenceUtils.setSoundPlayData(sounds);
    }

    public static List<SoundModel> getSoundDataPlay() {
        String sounds = PreferenceUtils.getSoundPlayData();
        if (sounds.matches("")){
            return null;
        }
        List<SoundModel> soundDatas = Arrays.asList(getGson().fromJson(sounds, SoundModel[].class));
        return soundDatas;
    }

    public static List<SoundModel> mapListSoundSave(List<SoundModel> soundData, List<SoundModel> soundDataSave){
        if (soundDataSave != null && soundData.size() > 0){
            for (int i = 0; i < soundDataSave.size(); i++) {
                for (int j = 0; j < soundData.size(); j++)
                    if (soundDataSave.get(i).getId() == soundData.get(j).getId()) {
                        soundData.set(i, soundDataSave.get(i));
                    }
            }
        }
        return soundData;
    }

    public static List<SoundModel> getDataSound(){
        List<SoundModel> listSound = new ArrayList<>();
        for (int i = 0; i < SoundEnum.values().length; i++) {
            SoundEnum soundEnum = SoundEnum.values()[i];
            listSound.add(new SoundModel(i, null, soundEnum.getSoundName(), soundEnum.getSoundIconResId(),
                    "1", soundEnum.getSoundResId(), "", "", 0.5F, false));
        }
        return listSound;
    }

    public static List<SoundModel> getListSoundSave(List<SoundModel> soundData, List<SoundModel> soundDataSave){
        List<SoundModel> listSoundTemp = new ArrayList<>();
        if (soundDataSave != null && soundData.size() > 0){
            for (int i = 0; i < soundDataSave.size(); i++) {
                for (int j = 0; j < soundData.size(); j++)
                    if (soundDataSave.get(i).getId() == soundData.get(j).getId()) {
                        soundData.get(j).setPlay(true);
                        listSoundTemp.add(soundData.get(j));
                    }
            }
        }
        return listSoundTemp;
    }


    public static List<SoundGroupModel> getSoundGroupFromCategoryId(ArrayList<SoundGroupModel> listSound, int categoryId){
        List<SoundGroupModel> soundDatas = new ArrayList<>();
        for (SoundGroupModel sound :
                listSound) {
            if (categoryId == sound.getCategoryId())
                soundDatas.add(sound);
        }
        return soundDatas;
    }

    public static SoundModel getSoundFromId(int id, ArrayList<SoundModel> soundData){
        SoundModel sound = null;
        for (int i = 0; i < soundData.size(); i++) {
            if (id == soundData.get(i).getId()){
                sound = soundData.get(i);
            }
        }
        return sound;
    }

    public static Resources getRes(){
        return MyApplication.getInstance().getResources();
    }
}
