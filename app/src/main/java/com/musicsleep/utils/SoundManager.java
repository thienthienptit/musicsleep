package com.musicsleep.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.musicsleep.model.SoundModel;
import com.musicsleep.service.ServiceManager;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class SoundManager {
    private Context context;
    private ArrayList<SoundModel> soundData = new ArrayList<>();
    private OnCreateMultipleSound onCreateMultipleSound;
    private boolean isPlayNormal = true;

    public SoundManager(Context context) {
        this.context = context;
    }

    public MediaPlayer createSoundRes(int resId) {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, resId);
        return mediaPlayer;
    }

    public MediaPlayer createSoundOnline(String url) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mediaPlayer;
    }

    public MediaPlayer createSoundLocal(Uri uri) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(context, uri);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mediaPlayer;
    }

    public void createMultipleSound(ArrayList<SoundModel> listSound, OnCreateMultipleSound onCreateMultipleSound) {
        createMultipleSound(true, listSound, onCreateMultipleSound);
    }

    public void createMultipleSound(boolean isPlayNormal, ArrayList<SoundModel> listSound,
                                    OnCreateMultipleSound onCreateMultipleSound) {
        setPlayNormal(isPlayNormal);
        stopMultipleSound();
        this.soundData.clear();
        MediaPlayer mediaPlayer = null;
        this.onCreateMultipleSound = onCreateMultipleSound;
        if (listSound == null || listSound.size() == 0) return;
        for (SoundModel soundModel :
                listSound) {
            switch (soundModel.getType()) {
                case "1":
                    mediaPlayer = createSoundRes(soundModel.getResId());
                    break;
                case "2":
                    mediaPlayer = createSoundOnline(soundModel.getUrl());
                    break;
                case "3":
                    mediaPlayer = createSoundLocal(Uri.parse(soundModel.getPart()));
                    break;
            }
            soundModel.setMediaPlayer(mediaPlayer);
            soundData.add(soundModel);
        }
        if (onCreateMultipleSound != null)
            onCreateMultipleSound.onSuccess(listSound);
        Log.e("SoundData", soundData.size() + "");
    }

    public void stopSoundFromMedia(MediaPlayer mediaPlayer){
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.reset();
        }
    }

    public void playMultipleSound() {
        if (soundData.size() > 0) {
            for (SoundModel soundModel :
                    soundData) {
                if (soundModel.getMediaPlayer() != null && !soundModel.getMediaPlayer().isPlaying())
                    soundModel.getMediaPlayer().start();
            }
        }
    }

    public void pauseMultipleSound(){
        if (soundData.size() > 0) {
            for (SoundModel soundModel :
                    soundData) {
                if (soundModel.getMediaPlayer() != null && soundModel.getMediaPlayer().isPlaying()) {
                    soundModel.getMediaPlayer().pause();
                }
            }
        }
    }

    public void stopMultipleSound() {
        if (soundData.size() > 0) {
            for (SoundModel soundModel :
                    soundData) {
                if (soundModel.getMediaPlayer() != null && soundModel.getMediaPlayer().isPlaying()) {
                    soundModel.getMediaPlayer().stop();
                    soundModel.getMediaPlayer().reset();
                }
            }
        }
    }

    public boolean isSoundPlaying() {
        if (soundData.size() > 0) {
            for (SoundModel soundModel :
                    soundData) {
                if (soundModel.getMediaPlayer().isPlaying()) return true;
            }
        }
        return false;
    }

    public void addSound(SoundModel soundModel){
        if (!isSoundPlaying() && soundData.size() > 0){
            playMultipleSound();
        }
        MediaPlayer mediaPlayer = null;
        switch (soundModel.getType()) {
            case "1":
                mediaPlayer = createSoundRes(soundModel.getResId());
                break;
            case "2":
                mediaPlayer = createSoundOnline(soundModel.getUrl());
                break;
            case "3":
                mediaPlayer = createSoundLocal(Uri.parse(soundModel.getPart()));
                break;
        }
        soundModel.setMediaPlayer(mediaPlayer);
        soundModel.getMediaPlayer().start();
        soundModel.getMediaPlayer().setLooping(true);
        this.soundData.add(soundModel);
        if (onCreateMultipleSound != null)
            onCreateMultipleSound.onSuccess(soundData);
    }

    public void removeSound(SoundModel soundModel){
        if (soundData.size() > 0) {
            stopSoundFromMedia(soundModel.getMediaPlayer());
            for (int i = 0; i < soundData.size(); i++) {
                if (soundModel.getId() == soundData.get(i).getId())
                    soundData.remove(i);
            }
        }else if (soundData.size() == 1){
            stopSoundFromMedia(soundModel.getMediaPlayer());
            soundData.clear();
        }
        Log.e("SoundData", soundData.size() + "");
    }

    public interface OnCreateMultipleSound{
        void onSuccess(ArrayList<SoundModel> listSound);
    }

    public ArrayList<SoundModel> getSoundData() {
        for (SoundModel sound :
                soundData) {
            sound.setPlay(true);
        }
        return soundData;
    }

    public void setSoundData(ArrayList<SoundModel> soundData) {
        this.soundData = soundData;
    }

    public boolean isPlayNormal() {
        return isPlayNormal;
    }

    public void setPlayNormal(boolean playNormal) {
        isPlayNormal = playNormal;
    }
}
