package com.musicsleep.base;

import com.musicsleep.model.SoundCategoryModel;
import com.musicsleep.model.SoundGroupModel;
import com.musicsleep.model.SoundModel;
import com.musicsleep.utils.SoundManager;

import java.util.List;

public interface BaseController {
    void createSound(List<SoundModel> soundModels);

    void createSound(List<SoundModel> soundModels, boolean isPlayNormal);

    void addSound(SoundModel soundModel);

    void removeSound(SoundModel soundModel);

    void pauseSound();

    void playSound();

    boolean isPlaySound();

    boolean isPlayNormal();

    int getSoundQuantity();

    List<SoundModel> getListSoundPlay();

    List<SoundModel> getSoundDatas();

    List<SoundGroupModel> getSoundGroupDatas();

    List<SoundCategoryModel> getSoundCategoryDatas();


}
