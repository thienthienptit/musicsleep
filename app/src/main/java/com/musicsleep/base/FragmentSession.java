package com.musicsleep.base;

import android.support.v4.app.Fragment;

public class FragmentSession {
    private static FragmentSession instance;

    public static FragmentSession getInstance() {
        if (instance == null) {
            instance = new FragmentSession();
        }
        return instance;
    }

    public Fragment fragment;
    public String tag;

    public void setData(Fragment fragment) {
        this.fragment = fragment;
    }
}
