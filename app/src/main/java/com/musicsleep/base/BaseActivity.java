package com.musicsleep.base;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.musicsleep.data.DataResource;
import com.musicsleep.model.SoundCategoryModel;
import com.musicsleep.model.SoundGroupModel;
import com.musicsleep.model.SoundModel;
import com.musicsleep.service.ServiceManager;
import com.musicsleep.utils.SoundManager;
import com.musicsleep.utils.Toolbox;

import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends AppCompatActivity implements BaseController {
    DataResource dataResource;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getDataResource() == null)
            setDataResource(new DataResource(this));
    }

    @Override
    public void createSound(List<SoundModel> soundModels) {
        ServiceManager.getInstance().createSound(soundModels, true);
    }

    @Override
    public void createSound(List<SoundModel> soundModels, boolean isPlayNormal) {
        ServiceManager.getInstance().createSound(soundModels, isPlayNormal);
    }

    @Override
    public void addSound(SoundModel soundModel) {
        ServiceManager.getInstance().addSound(soundModel);
    }

    @Override
    public void removeSound(SoundModel soundModel) {
        ServiceManager.getInstance().removeSound(soundModel);
    }

    @Override
    public void pauseSound() {
        ServiceManager.getInstance().pauseSound();
    }

    @Override
    public void playSound() {
        ServiceManager.getInstance().playSound();
    }

    @Override
    public boolean isPlaySound() {
        return ServiceManager.getInstance().isPlaySound();
    }

    @Override
    public boolean isPlayNormal() {
        return ServiceManager.getInstance().isPlayNormal();
    }

    @Override
    public int getSoundQuantity() {
        return ServiceManager.getInstance().getSoundQuantity();
    }

    @Override
    public List<SoundModel> getListSoundPlay() {
        return ServiceManager.getInstance().getListSound();
    }


    @Override
    public List<SoundModel> getSoundDatas() {
        return dataResource.getDataSound();
    }

    @Override
    public List<SoundGroupModel> getSoundGroupDatas() {
        return dataResource.getSoundGroupDatas();
    }

    @Override
    public List<SoundCategoryModel> getSoundCategoryDatas() {
        return dataResource.getSoundCategoryDatas();
    }

    public DataResource getDataResource() {
        return dataResource;
    }

    public void setDataResource(DataResource dataResource) {
        this.dataResource = dataResource;
    }
}
