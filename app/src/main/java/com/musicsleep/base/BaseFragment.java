package com.musicsleep.base;

import android.support.v4.app.Fragment;

import com.musicsleep.base.BaseActivity;
import com.musicsleep.base.BaseController;
import com.musicsleep.model.SoundCategoryModel;
import com.musicsleep.model.SoundGroupModel;
import com.musicsleep.model.SoundModel;
import com.musicsleep.utils.SoundManager;

import java.util.List;

public class BaseFragment extends Fragment implements BaseController {
    @Override
    public void createSound(List<SoundModel> soundModels) {
        ((BaseActivity) getActivity()).createSound(soundModels);
    }

    @Override
    public void createSound(List<SoundModel> soundModels, boolean isPlayNormal) {
        ((BaseActivity) getActivity()).createSound(soundModels, isPlayNormal);
    }

    @Override
    public void addSound(SoundModel soundModel) {
        ((BaseActivity) getActivity()).addSound(soundModel);
    }

    @Override
    public void removeSound(SoundModel soundModel) {
        ((BaseActivity) getActivity()).removeSound(soundModel);
    }

    @Override
    public void pauseSound() {
        ((BaseActivity) getActivity()).pauseSound();
    }

    @Override
    public void playSound() {
        ((BaseActivity) getActivity()).playSound();
    }

    @Override
    public boolean isPlaySound() {
        return ((BaseActivity) getActivity()).isPlaySound();
    }

    @Override
    public boolean isPlayNormal() {
        return ((BaseActivity) getActivity()).isPlayNormal();
    }

    @Override
    public int getSoundQuantity() {
        return ((BaseActivity) getActivity()).getSoundQuantity();
    }

    @Override
    public List<SoundModel> getListSoundPlay() {
        return ((BaseActivity) getActivity()).getListSoundPlay();
    }

    @Override
    public List<SoundModel> getSoundDatas() {
        return ((BaseActivity) getActivity()).getSoundDatas();
    }

    @Override
    public List<SoundGroupModel> getSoundGroupDatas() {
        return ((BaseActivity) getActivity()).getSoundGroupDatas();
    }

    @Override
    public List<SoundCategoryModel> getSoundCategoryDatas() {
        return ((BaseActivity) getActivity()).getSoundCategoryDatas();
    }
}
