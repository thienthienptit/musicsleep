package com.musicsleep.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.musicsleep.R;
import com.musicsleep.adapter.FragmentPagerAdapter;
import com.musicsleep.base.BaseActivity;
import com.musicsleep.data.RealmController;
import com.musicsleep.model.SoundModel;
import com.musicsleep.screen.CategorySoundContainerFragment;
import com.musicsleep.screen.SoundHomeFragment;
import com.musicsleep.service.ServiceManager;
import com.musicsleep.utils.SoundManager;
import com.musicsleep.utils.Toolbox;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class MainActivity extends BaseActivity {
    private static final int PERMISSION_REQUEST_STORAGE = 1001;

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    BottomNavigationView tabs;

    private FragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Intent intent = new Intent(this, ServiceManager.class);
        startService(intent);
        initUI();
        bindData();
    }

    private void bindData() {
        adapter = new FragmentPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(CategorySoundContainerFragment.newInstance(), "");
        adapter.addFragment(SoundHomeFragment.newInstance(), "");
        adapter.addFragment(CategorySoundContainerFragment.newInstance(), "");
        viewPager.setAdapter(adapter);
    }

    private void initUI() {
        tabs.setItemIconTintList(null);
        tabs.setOnNavigationItemSelectedListener(item -> {
            onNavigationSelect(item);
            return true;
        });
    }

    private void onNavigationSelect(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_category:
                viewPager.setCurrentItem(0);
                break;
            case R.id.navigation_sound_home:
                viewPager.setCurrentItem(1);
                break;
            case R.id.navigation_setting:
                viewPager.setCurrentItem(2);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    public void requestSmsPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Cần có quyền thực hiện cuộc gọi");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("Vui lòng xác nhận");
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}
                                    , PERMISSION_REQUEST_STORAGE);
                        }
                    });
                    builder.show();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSION_REQUEST_STORAGE);
                }
            } else {
            }
        } else {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission has been granted by user", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Permission has been denied by user", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
