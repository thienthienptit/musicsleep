package com.musicsleep.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.musicsleep.R;
import com.musicsleep.base.BaseActivity;
import com.musicsleep.base.FragmentSession;

import java.util.List;

public class FunctionActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_function);
        if (FragmentSession.getInstance().fragment != null) {
            addFragment(FragmentSession.getInstance().fragment);
        }
    }

    public void addFragment(Fragment targetFragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (targetFragment != null) {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            if (fragments == null || fragments.size() == 0) {
                ft.replace(R.id.fragment_container, targetFragment).commitAllowingStateLoss();
            } else {
                ft.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
                ft.addToBackStack(null);
                ft.add(R.id.fragment_container, targetFragment).commitAllowingStateLoss();
            }
        }
    }
}
