package com.musicsleep.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.musicsleep.R;
import com.musicsleep.model.SoundModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SoundViewAdapter extends BaseRecyclerAdapter<SoundModel, SoundViewAdapter.ViewHolder> {
    private OnClickItem onClickItem;

    public SoundViewAdapter(Context context, List<SoundModel> list) {
        super(context, list);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(list.get(position));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_sound, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_sound)
        ImageView ivSound;
        @BindView(R.id.seekbar)
        SeekBar seekbar;
        @BindView(R.id.tv_sound_name)
        TextView tvSoundName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(SoundModel soundModel){
            if (soundModel != null) {
                tvSoundName.setText(soundModel.getSoundName());
                seekbar.setVisibility(soundModel.isPlay() ? View.VISIBLE : View.INVISIBLE);
                seekbar.setMax(GridSoundViewAdapter.MAX_VOLUMN);
                seekbar.setProgress((int) (soundModel.getVolumn() * GridSoundViewAdapter.MAX_VOLUMN));
                seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        soundModel.setVolumn((float) progress / GridSoundViewAdapter.MAX_VOLUMN);
                        soundModel.getMediaPlayer().setVolume(soundModel.getVolumn(), soundModel.getVolumn());
                        if (onClickItem != null) {
                            onClickItem.onChangeSeekbar(soundModel);
                        }

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                itemView.setOnClickListener(v -> {
                    soundModel.setPlay(!soundModel.isPlay());
                    if (onClickItem != null)
                        if (soundModel.isPlay()) {
                            onClickItem.addSound(soundModel);
                        } else {
                            onClickItem.removeSound(soundModel);
                        }
                    notifyDataSetChanged();
                });
            }
        }
    }

    public interface OnClickItem {
        void onChangeSeekbar(SoundModel soundModel);

        void onSelectPlay(int soundId);

        void addSound(SoundModel soundModel);

        void removeSound(SoundModel soundModel);
    }

    public OnClickItem getOnClickItem() {
        return onClickItem;
    }

    public void setOnClickItem(OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }
}
