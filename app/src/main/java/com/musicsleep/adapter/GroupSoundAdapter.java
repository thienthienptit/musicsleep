package com.musicsleep.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.musicsleep.R;
import com.musicsleep.model.SoundGroupModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupSoundAdapter extends BaseRecyclerAdapter<SoundGroupModel, GroupSoundAdapter.ViewHolder> {

    public GroupSoundAdapter(Context context, List<SoundGroupModel> list) {
        super(context, list);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(list.get(position));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_group_sound, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_group_sound)
        ImageView ivGroupSound;
        @BindView(R.id.tv_group_name)
        TextView tvGroupName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(SoundGroupModel soundGroup) {
            ivGroupSound.setImageResource(soundGroup.getAvatar());
            tvGroupName.setText(soundGroup.getName());
        }
    }
}
