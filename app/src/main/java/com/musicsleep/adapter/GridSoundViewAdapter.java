package com.musicsleep.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.musicsleep.R;
import com.musicsleep.model.SoundModel;
import com.musicsleep.customview.SoundViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GridSoundViewAdapter extends BaseAdapter {
    private OnClickItem onClickItem;
    public static final int MAX_VOLUMN = 50;
    private List<SoundModel> dataList;

    public GridSoundViewAdapter(List<SoundModel> datas, int page) {
        dataList = new ArrayList<>();
        int start = page * SoundViewPager.GRID_NUMBER_ITEM;
        int end = start + SoundViewPager.GRID_NUMBER_ITEM;
        while ((start < datas.size()) && (start < end)) {
            dataList.add(datas.get(start));
            start++;
        }
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sound, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final SoundModel soundModel = dataList.get(position);
        if (soundModel != null) {
            holder.tvSoundName.setText(soundModel.getSoundName());
            holder.seekbar.setVisibility(soundModel.isPlay() ? View.VISIBLE : View.INVISIBLE);
            holder.seekbar.setMax(MAX_VOLUMN);
            holder.seekbar.setProgress((int) (soundModel.getVolumn() * MAX_VOLUMN));
            holder.seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    soundModel.setVolumn((float) progress / MAX_VOLUMN);
                    soundModel.getMediaPlayer().setVolume(soundModel.getVolumn(), soundModel.getVolumn());
                    if (onClickItem != null) {
                        onClickItem.onChangeSeekbar(soundModel);
                    }

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            holder.itemView.setOnClickListener(v -> {
                soundModel.setPlay(!soundModel.isPlay());
                if (onClickItem != null)
                    if (soundModel.isPlay()) {
                        onClickItem.addSound(soundModel);
                    } else {
                        onClickItem.removeSound(soundModel);
                    }
                notifyDataSetChanged();
            });
        }
        return convertView;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_sound)
        ImageView ivSound;
        @BindView(R.id.seekbar)
        SeekBar seekbar;
        @BindView(R.id.tv_sound_name)
        TextView tvSoundName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnClickItem {
        void onChangeSeekbar(SoundModel soundModel);

        void onSelectPlay(int soundId);

        void addSound(SoundModel soundModel);

        void removeSound(SoundModel soundModel);
    }

    public OnClickItem getOnClickItem() {
        return onClickItem;
    }

    public void setOnClickItem(OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }
}
