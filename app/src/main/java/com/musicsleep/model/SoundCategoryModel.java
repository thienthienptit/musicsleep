package com.musicsleep.model;

import com.musicsleep.data.enum_sound.SoundCategoryEnum;

public class SoundCategoryModel {
    private int id;
    private String name;
    private int iconResId;

    public SoundCategoryModel(int id, String name, int iconResId) {
        this.id = id;
        this.name = name;
        this.iconResId = iconResId;
    }

    public SoundCategoryModel(SoundCategoryEnum category){
        this.id = category.getId();
        this.name = category.getName();
        this.iconResId = category.getIconResId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIconResId() {
        return iconResId;
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
    }
}
