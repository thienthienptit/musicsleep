package com.musicsleep.model;

import com.musicsleep.data.enum_sound.SoundGroupEnum;
import com.musicsleep.data.realm_object.SoundGroupObject;

public class SoundGroupModel {
    private int id;
    private String name;
    private int avatar;
    private int categoryId;
    private String listSoundId;

    public SoundGroupModel(int id, String name, int avatar, int categoryId, String listSoundId) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.categoryId = categoryId;
        this.listSoundId = listSoundId;
    }

    public SoundGroupModel(SoundGroupObject sound) {
        this.id = sound.getId();
        this.name = sound.getName();
        this.avatar = sound.getAvatar();
        this.categoryId = sound.getCategoryId();
        this.listSoundId = sound.getListSoundId();
    }

    public SoundGroupModel(SoundGroupEnum sound){
        this.id = sound.getId();
        this.name = sound.getName();
        this.avatar = sound.getAvatar();
        this.categoryId = sound.getCategoryId();
        this.listSoundId = sound.getListSoundId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getListSoundId() {
        return listSoundId;
    }

    public void setListSoundId(String listSoundId) {
        this.listSoundId = listSoundId;
    }
}
