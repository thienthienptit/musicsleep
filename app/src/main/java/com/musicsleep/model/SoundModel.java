package com.musicsleep.model;

import android.media.MediaPlayer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SoundModel{
    @Expose
    public int id;
    private MediaPlayer mediaPlayer;
    @Expose
    private String soundName;
    @Expose
    private int soundIcon;
    @Expose
    private String type;
    @Expose
    private int resId;
    @Expose
    private String part;
    @Expose
    private String url;
    @Expose
    private float volumn;
    @Expose
    private boolean isPlay;

    public SoundModel(int id, MediaPlayer mediaPlayer, String soundName, int soundIcon, String type,
                      int resId, String part, String url, float volumn, boolean isPlay) {
        this.id = id;
        this.mediaPlayer = mediaPlayer;
        this.soundName = soundName;
        this.soundIcon = soundIcon;
        this.type = type;
        this.resId = resId;
        this.part = part;
        this.url = url;
        this.volumn = volumn;
        this.isPlay = isPlay;
    }

    public SoundModel(int id, MediaPlayer mediaPlayer, String type, int resId, String part, String url, float volumn) {
        this.id = id;
        this.mediaPlayer = mediaPlayer;
        this.type = type;
        this.resId = resId;
        this.part = part;
        this.url = url;
        this.volumn = volumn;
    }

    public SoundModel() {
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getVolumn() {
        return volumn;
    }

    public void setVolumn(float volumn) {
        this.volumn = volumn;
    }

    public String getSoundName() {
        return soundName;
    }

    public void setSoundName(String soundName) {
        this.soundName = soundName;
    }

    public int getSoundIcon() {
        return soundIcon;
    }

    public void setSoundIcon(int soundIcon) {
        this.soundIcon = soundIcon;
    }

    public boolean isPlay() {
        return isPlay;
    }

    public void setPlay(boolean play) {
        isPlay = play;
    }
}
