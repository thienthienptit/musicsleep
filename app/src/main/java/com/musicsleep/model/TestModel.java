package com.musicsleep.model;

import com.google.gson.annotations.SerializedName;

public class TestModel {
    @SerializedName("id")
    private int id;

    public TestModel(int id) {
        this.id = id;
    }
}
