package com.musicsleep;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.musicsleep.data.DataResource;

import io.realm.Realm;

public class MyApplication extends Application {
    private static Context instance;
    private static Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(this);
        Realm.init(this);
        DataResource dataResource = new DataResource(this);
        dataResource.initData();
        gson = new Gson();
    }

    public static Context getInstance() {
        return instance;
    }

    public static void setInstance(Context instance) {
        MyApplication.instance = instance;
    }

    public static Gson getGson() {
        return gson;
    }

    public static void setGson(Gson gson) {
        MyApplication.gson = gson;
    }
}
