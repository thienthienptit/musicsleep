package com.musicsleep.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.browse.MediaBrowser;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.service.media.MediaBrowserService;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.musicsleep.base.BaseActivity;
import com.musicsleep.base.BaseController;
import com.musicsleep.R;
import com.musicsleep.model.SoundModel;
import com.musicsleep.utils.SoundManager;
import com.musicsleep.utils.Toolbox;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("NewApi")
public class ServiceManager extends MediaBrowserService{
    public static ServiceManager instance;

    private static final int ID_NOTIFI = 10000;
    private Notification notification;
    private Notification.Builder builder;
    private NotificationChannel notificationChannel;
    private NotificationManager notificationmanager;
    private SoundManager soundManager;

    public static ServiceManager getInstance() {
        return instance;
    }

    public static void setInstance(ServiceManager instance) {
        ServiceManager.instance = instance;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_NOT_STICKY;
        }

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(ServiceManager.this);
        soundManager = new SoundManager(this);
        showNotification(getApplicationContext());
    }

    public void showNotification(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notification = new Notification.Builder(getApplicationContext(), ID_NOTIFI + "").build();
        } else {
            notification = new Notification.Builder(getApplicationContext()).build();
        }
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notification.icon = android.R.drawable.ic_media_play;
        notification.visibility = NotificationCompat.VISIBILITY_PUBLIC;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(ID_NOTIFI + ""
                    , "NOTIFICATION_CHANNEL_NAME"
                    , NotificationManager.IMPORTANCE_HIGH);
            // Sets whether notifications posted to this channel should display notification lights
            notificationChannel.enableLights(false);
            // Sets whether notification posted to this channel should vibrate.
            notificationChannel.enableVibration(false);
            notificationChannel.setSound(null, null);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationmanager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationmanager.createNotificationChannel(notificationChannel);
            notificationmanager.notify(ID_NOTIFI, notification);
            startForeground(ID_NOTIFI, notification);
        } else {
            startForeground(ID_NOTIFI, notification);
        }
    }

    public void createSound(List<SoundModel> soundModels, boolean isPlayNormal) {
        soundManager.createMultipleSound(isPlayNormal, (ArrayList<SoundModel>) soundModels, listSound -> {
        });
    }

    public void addSound(SoundModel soundModel) {
        soundManager.addSound(soundModel);
    }

    public void removeSound(SoundModel soundModel) {
        soundManager.removeSound(soundModel);
    }

    public void pauseSound() {
        soundManager.pauseMultipleSound();
    }

    public void playSound() {
        soundManager.playMultipleSound();
    }

    public boolean isPlaySound(){
        return soundManager.isSoundPlaying();
    }

    public int getSoundQuantity(){
        return soundManager == null ? 0 : soundManager.getSoundData().size();
    }

    public List<SoundModel> getListSound(){
        return soundManager.getSoundData();
    }

    public boolean isPlayNormal(){
        return soundManager.isPlayNormal();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("Service", "OnDestroy");
    }

    @Nullable
    @Override
    public BrowserRoot onGetRoot(@NonNull String clientPackageName, int clientUid, @NonNull Bundle rootHints) {
        if (TextUtils.equals(clientPackageName, getPackageName())) {
            return new BrowserRoot(getString(R.string.app_name), null);
        }

        return null;
    }

    @Override
    public void onLoadChildren(@NonNull String parentId, @NonNull MediaBrowserService.Result<List<MediaBrowser.MediaItem>> result) {
        result.sendResult(null);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void setSoundManager(SoundManager soundManager) {
        soundManager.playMultipleSound();
    }

    public class Binder extends android.os.Binder {
        public ServiceManager getService() {
            return ServiceManager.this;
        }
    }
}
